import { Injectable } from '@angular/core';


@Injectable()
export class ConfigService {
  // youtube api key
  private youtubeApiKey = 'ENTER-YOUTUBE-API-KEY-HERE';

  getYouTubeAPIKey() {
    return this.youtubeApiKey;
  }
}
