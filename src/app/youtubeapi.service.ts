import { Http, RequestOptions, Response , URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from './config.service';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

const API_URL = 'https://www.googleapis.com/youtube/v3/search';

@Injectable()
export class YouTubeAPIService {
  videos: Array<object>;


  constructor(private http: Http, private config: ConfigService) { }

  // do service init here
  init() {

  }

  search(term: string) {
    const params: URLSearchParams = new URLSearchParams();
    params.set('part', 'snippet');
    params.set('key', this.config.getYouTubeAPIKey());
    params.set('q', term);
    params.set('type', 'video');

    const requestOptions: RequestOptions = new RequestOptions({
      search: params
    });

    // TODO: handle errors

    return this.http.get(API_URL, requestOptions)
      .map( (res) => {
        return res.json();
      })
      .debounceTime(500);
  }


}
