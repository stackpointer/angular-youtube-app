import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoListItemComponent } from './video-list-item/video-list-item.component';
import { YouTubeAPIService } from './youtubeapi.service';
import { VideoService } from './video.service';
import { ConfigService } from './config.service';


@NgModule({
  declarations: [
    AppComponent,
    /* add custom components */
    VideoListComponent,
    SearchBarComponent,
    VideoDetailComponent,
    VideoListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [YouTubeAPIService, VideoService, ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
