import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class VideoService {

  private videoSelectedSource = new Subject<any>();
  // app component will subscribe to observable to be notified on
  // video selection changes
  videoSelected$ = this.videoSelectedSource.asObservable();


  // video-list-item component will update selected video
  selectVideo(video) {
    this.videoSelectedSource.next(video);
  }
}
