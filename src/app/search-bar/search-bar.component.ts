import {Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { Event } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent implements OnInit, OnDestroy {
  @Output() searchTermChanged = new EventEmitter<string>();
  @Input() defaultSearchTerm: string;
  @ViewChild('searchInput') searchInput: ElementRef;
  debounceDelay: number = 500;
  searchTermEventStream;

  term = '';


  constructor() { }

  ngOnInit() {
    this.term = this.defaultSearchTerm || '';

    this.searchTermEventStream = Observable.fromEvent(this.searchInput.nativeElement,'input')
      .debounceTime(this.debounceDelay);

    this.searchTermEventStream.subscribe(
      (e) => {
        this.term = e.target.value;
        this.searchTermChanged.emit(this.term);
    });

  }

  ngOnDestroy () {
    this.searchTermEventStream.unsubscribe();
  }


  onSearchTermChange(event: any) {
    // console.log("onSearchTermChange() called");
    // console.log(event);
  }
}
