import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { YouTubeAPIService } from './youtubeapi.service';
import { VideoService } from './video.service';
import { Subscription }    from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  // selector: '[app-root]', // if using <div app-root></div>
  // selector: '.app-root',  // if using <div class="app-root"></div>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  name = 'hello';
  defaultSearchTerm = 'angular2';
  videoList: Array<object>;
  selectedVideo: object;
  videoSubscription: Subscription;

  constructor (private youtubeApiService: YouTubeAPIService, private videoService: VideoService) {
    this.videoSubscription = videoService.videoSelected$.subscribe(
      (video) => {
        this.selectedVideo = video;
      });
  }

  ngOnInit () {
    this.youtubeApiService.search(this.defaultSearchTerm)
      .subscribe(res => {
        this.videoList = res.items;
        this.selectedVideo = this.videoList[0];
      });
  }

  ngOnDestroy() {
    this.videoSubscription.unsubscribe();
  }

  onSearchTermChange(term: string) {
    // NOTE: async call, define callback in subscribe()
    this.youtubeApiService.search(term)
      .subscribe( res => {
        this.videoList = res.items;
        this.selectedVideo = this.videoList[0];
        // console.log("here");
        // console.log(res);
        // console.log(this.videoList);
        // console.log(this.selectedVideo);
      });

    // NOTE: console.log below should give undefined as this.videos is set asynchronously
    // console.log(this.videos);
  }



}
