import {Component, OnInit, Output, Input, SimpleChanges, EventEmitter, OnChanges} from '@angular/core';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-video-list-item',
  templateUrl: './video-list-item.component.html',
  styleUrls: ['./video-list-item.component.css']
})
export class VideoListItemComponent implements OnInit, OnChanges {
  @Input() video: any;
  imageUrl: string;
  videoTitle: string;

  constructor(private videoService: VideoService) {
  }

  ngOnInit() {
    // console.log(this.video);
    this.imageUrl = this.video.snippet.thumbnails.default.url;
    this.videoTitle = this.video.snippet.title;
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  onVideoSelect(event: Event) {
    this.videoService.selectVideo(this.video);
  }

}
