# Angular YouTube App

A simple YouTube search app using [Angular](https://angular.io/)

## Introduction

YouTube app written in Angular2 that uses YouTube v3 Data API to fetch and display results.

![preview](screenshot/screenshot01.jpg)

## Installation

1. Checkout repo and  install dependencies
```
$ git clone https://gitlab.com/stackpointer/angular-youtube-app.git
$ cd angular-youtube-app
$ npm install
```

2. Copy `src/app/config-dist.service.ts` to `src/app/config.service.ts`

3. Edit the `src/app/config.service.ts` file and replace the string `ENTER-YOUTUBE-API-KEY-HERE` with your own YouTube API v3 key. You can get the API key for free from Google Developers Console. Refer to Google's [Getting Started](https://developers.google.com/youtube/v3/getting-started) guide for more info.
 
4. Start the application
```
$ ng serve
```

5. Launch browser at `http://localhost:4200`

## Features

- Display first 5 search results
- Select video to be played from search results

## Copyright and License
Copyright (c) 2017, Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.
